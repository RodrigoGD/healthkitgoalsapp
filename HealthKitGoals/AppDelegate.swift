//
//  AppDelegate.swift
//  HealthKitGoals
//
//  Created by Rodrigo Gálvez on 28/8/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupInitialViewController()
        return true
    }

    private func setupInitialViewController() {
        window = UIWindow(frame: UIScreen.main.bounds)

        let storyBoard = UIStoryboard(name: "GoalList", bundle: nil)
        let controller = storyBoard.instantiateViewController(identifier: "GoalListViewController")

        let navigationController = UINavigationController(rootViewController: controller)

        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
}

