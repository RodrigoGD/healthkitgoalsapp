//
//  GoalRepositoryImplementation.swift
//  HealthKitGoals
//
//  Created by Rodrigo Gálvez on 29/8/21.
//

import Foundation

class GoalRepositoryImplementation: GoalRepository {

    private let goalApi: GoalApi!
    private let goalFileManager: GoalFileManager!

    init(_ goalApi: GoalApi = GoalApi(),
         _ goalFileManager: GoalFileManager = GoalFileManager()) {
        self.goalApi = goalApi
        self.goalFileManager = goalFileManager
    }

    func getGoals(completion: @escaping (Result<[Goal], DomainError>) -> Void) {
        requestGoalsFromNetwork { result in
            switch result {
            case .success(let goals):
                self.saveGoalsToFile(goals: goals)
                completion(.success(goals.map { $0.toDomain() }))
            case .failure(let error):
                print(error.localizedDescription)
                self.retrieveGoalsFromLocal { result in
                    switch result {
                    case .success(let goals):
                        completion(.success(goals.map { $0.toDomain() }))
                    case .failure(let error):
                        completion(.failure(error.toDomain()))
                    }
                }
            }
        }
    }

    func getGoal(id: Int, completion: @escaping (Result<Goal, DomainError>) -> Void) {
        retrieveGoalsFromLocal { result in
            switch result {
            case .success(let goals):
                guard let goal = goals.filter({ $0.id == id }).first else {
                    return completion(.failure(DomainError(message: "Goal not found")))
                }
                completion(.success(goal.toDomain()))
            case .failure(let error):
                completion(.failure(error.toDomain()))
            }
        }
    }

    // MARK: - Network
    private func requestGoalsFromNetwork(completion: @escaping (Result<[GoalDto], ApiError>) -> Void) {
        goalApi.requestGoals { result in
            switch result {
            case .success(let container):
                completion(.success(container.goals))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    // MARK: - Local
    private func retrieveGoalsFromLocal(completion: @escaping (Result<[GoalDto], FileManagerError>) -> Void) {
        goalFileManager.retrieveGoals { result in
            switch result {
            case .success(let goals):
                completion(.success(goals))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    private func saveGoalsToFile(goals: [GoalDto]) {
        goalFileManager.saveGoals(goals) { result in
            if case let .failure(error) = result { print(error.localizedDescription) }
        }
    }
}
