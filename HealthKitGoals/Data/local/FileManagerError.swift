//
//  FileManagerError.swift
//  HealthKitGoals
//
//  Created by Rodrigo Gálvez on 29/8/21.
//

import Foundation

enum FileManagerError: Error {
    case dataParsing
    case read
    case write

    func toDomain() -> DomainError {
        switch self {
        case .dataParsing:
            return DomainError(message: "Parse error")
        case .read:
            return DomainError(message: "Data cannot be retrieved")
        case .write:
            return DomainError(message: "Data cannot be save")
        }
    }
}
