//
//  GoalFileManager.swift
//  HealthKitGoals
//
//  Created by Rodrigo Gálvez on 29/8/21.
//

import Foundation

class GoalFileManager {
    private let file: String!

    init(file: String = "goals.json" ) {
        self.file = file
    }

    func saveGoals(_ goals: [GoalDto], completion: @escaping (Result<Bool, FileManagerError>) -> Void) {
        guard let json = serializeGoals(goals: goals) else { return completion(.failure(.dataParsing)) }

        guard let pathFilename = pathURL() else { return completion(.failure(.write)) }
        do {
            try json.write(to: pathFilename, atomically: true, encoding: .utf8)
            completion(.success(true))
        } catch {
            print(error)
            completion(.failure(.write))
        }
    }

    func retrieveGoals(completion: @escaping (Result<[GoalDto], FileManagerError>) -> Void) {
        guard let pathURL = pathURL() else {
            return completion(.failure(.read))
        }
        do {
            let data = try Data(contentsOf: pathURL)
            guard let goals = parse(jsonData: data) else { return completion(.failure(.dataParsing)) }
            completion(.success(goals))
        } catch {
            print(error.localizedDescription)
            completion(.failure(.read))
        }
    }

    func fileModificationDate(completion: @escaping (Result<Date, FileManagerError>) -> Void) {
        guard let pathURL = pathURL() else {
            return completion(.failure(.read))
        }
        do {
            let attr = try FileManager.default.attributesOfItem(atPath: pathURL.path)
            guard let date = attr[FileAttributeKey.modificationDate] as? Date else {
                return completion(.failure(.dataParsing))
            }
            completion(.success(date))
        } catch {
            return completion(.failure(.read))
        }
    }

    private func pathURL() -> URL? {
        guard let directory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return nil
        }
        let pathFilename = directory.appendingPathComponent(file)
        return pathFilename
    }

    private func serializeGoals(goals: [GoalDto]) -> String? {
        do {
            let jsonEncoder = JSONEncoder()
            let jsonData = try jsonEncoder.encode(goals)
            let json = String(data: jsonData, encoding: String.Encoding.utf8)
            return json
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }

    private func parse(jsonData: Data) -> [GoalDto]? {
        do {
            let decodedData = try JSONDecoder().decode([GoalDto].self, from: jsonData)
            return decodedData
        } catch {
            print(error.localizedDescription)
            return nil
        }

    }
}
