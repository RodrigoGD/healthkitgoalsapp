//
//  ApiError.swift
//  HealthKitGoals
//
//  Created by Rodrigo Gálvez on 28/8/21.
//

import Foundation

enum ApiError: Error {
    case dataParsing

    func toDomain() -> DomainError {
        switch self {
        case .dataParsing:
            return DomainError(message: "Parse error")
        }
    }
}
