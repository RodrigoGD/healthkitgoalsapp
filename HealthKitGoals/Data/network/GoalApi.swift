//
//  GoalApi.swift
//  HealthKitGoals
//
//  Created by Rodrigo Gálvez on 28/8/21.
//

import Foundation
import Alamofire

class GoalApi {
    private let baseUrl = "https://d9fd1bed-3c81-43a5-bb37-bc97488093f7.mock.pstmn.io"

    func requestGoals(completion: @escaping ((Result<GoalListContainer, ApiError>) -> Void)) {
        AF.request(baseUrl + "/goals").response { response in
            let decoder = JSONDecoder()

            guard let data = response.data, let dto = try? decoder.decode(GoalListContainer.self, from: data) else {
                return completion(.failure(.dataParsing))
            }

            completion(.success(dto))
        }
    }
}
