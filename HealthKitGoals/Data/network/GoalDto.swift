//
//  GoalDto.swift
//  HealthKitGoals
//
//  Created by Rodrigo Gálvez on 28/8/21.
//

import Foundation

struct GoalListContainer: Decodable {
    let goals: [GoalDto]

    enum CodingKeys: String, CodingKey {
        case goals = "goals"
    }
}

struct GoalDto: Codable {
    let id: Int
    let goal: String
    let description: String
    let title: String
    let type: GoalTypeDto
    let reward: RewardDto

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case goal = "goal"
        case description = "description"
        case title = "title"
        case type = "type"
        case reward = "reward"
    }

    func toDomain() -> Goal {
        return Goal(id: id,
                    goal: goal,
                    description: description,
                    title: title,
                    type: type.toDomain(),
                    reward: reward.toDomain())
    }
}

struct RewardDto: Codable {
    let trophy: TrophyDto
    let points: String

    enum CodingKeys: String, CodingKey {
        case trophy = "trophy"
        case points = "points"
    }

    func toDomain() -> Reward {
        return Reward(trophy: trophy.toDomain(),
                      points: points)
    }
}

enum GoalTypeDto: String, Codable {
    case step = "step"
    case walkingDistance = "walking_distance"
    case runningDistance = "running_distance"

    func toDomain() -> GoalType {
        switch self {
        case .step: return .step
        case .walkingDistance: return .walk
        case .runningDistance: return .run
        }
    }
}

enum TrophyDto: String, Codable {
    case bronzeMedal = "bronze_medal"
    case silverMedal = "silver_medal"
    case goldMedal = "gold_medal"
    case zombieHand = "zombie_hand"

    func toDomain() -> Trophy {
        switch self {
        case .bronzeMedal: return .bronze
        case .silverMedal: return .silver
        case .goldMedal: return .gold
        case .zombieHand: return .zombie
        }
    }
}
