//
//  DomainError.swift
//  HealthKitGoals
//
//  Created by Rodrigo Gálvez on 28/8/21.
//

import Foundation

struct DomainError: Error {
    let message: String
}
