//
//  Goal.swift
//  HealthKitGoals
//
//  Created by Rodrigo Gálvez on 28/8/21.
//

import Foundation

struct Goal {
    let id: Int
    let goal: String
    let description: String
    let title: String
    let type: GoalType
    let reward: Reward
}

struct Reward {
    let trophy: Trophy
    let points: String
}

enum GoalType: String {
    case step, walk, run
}

enum Trophy: String {
    case bronze, silver, gold, zombie
}
