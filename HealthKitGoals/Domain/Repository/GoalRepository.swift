//
//  GoalRepository.swift
//  HealthKitGoals
//
//  Created by Rodrigo Gálvez on 29/8/21.
//

import Foundation

protocol GoalRepository {
    func getGoals(completion: @escaping (Result<[Goal], DomainError>) -> Void)
    func getGoal(id: Int, completion: @escaping (Result<Goal, DomainError>) -> Void)
}
