//
//  HealthKitStore.swift
//  HealthKitGoals
//
//  Created by Rodrigo Gálvez on 28/8/21.
//

import HealthKit

class HealthKitStore {

    private enum HealthKitError: Error {
        case notAvailable
        case noDataType
        case unknown
    }

    static let sharedInstance = HealthKitStore()

    private lazy var datePredicate: NSPredicate = {
        let date = Date()
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        let newDate = cal.startOfDay(for: date)

        return HKQuery.predicateForSamples(withStart: newDate, end: Date(), options: .strictStartDate)
    }()

    func authorizeHealthKit(completion: @escaping(Result<Bool, Error>) -> Void) {
        guard HKHealthStore.isHealthDataAvailable() else {
            return completion(.failure(HealthKitError.notAvailable))
        }

        guard let stepCount = HKObjectType.quantityType(forIdentifier: .stepCount) else {
            return completion(.failure(HealthKitError.noDataType))
        }

        let objectsToRead: Set<HKObjectType> = [stepCount, HKObjectType.workoutType()]

        HKHealthStore().requestAuthorization(toShare: nil,
                                             read: objectsToRead) { (success, error) in
            guard success else {
                return completion(.failure(error ?? HealthKitError.unknown))
            }
            completion(.success(true))
        }
    }

    func getStepCount(completion: @escaping (Double) -> Void) {
        guard let type = HKSampleType.quantityType(forIdentifier: .stepCount) else {
            fatalError("Something went wrong retrieving quantity type step count")
        }

        let query = HKStatisticsQuery(quantityType: type,
                                      quantitySamplePredicate: datePredicate,
                                      options: [.cumulativeSum]) { (query, statistics, error) in
            var value: Double = 0

            if error != nil {
                print("something went wrong")
            } else if let quantity = statistics?.sumQuantity() {
                value = quantity.doubleValue(for: HKUnit.count())
            }
            DispatchQueue.main.async {
                completion(value)
            }
        }
        HKHealthStore().execute(query)
    }

    func getDistanceWalking(completion: @escaping (Double) -> Void) {
        let workoutPredicate = HKQuery.predicateForWorkouts(with: .walking)

        let compound = NSCompoundPredicate(andPredicateWithSubpredicates:
            [workoutPredicate, datePredicate])

        getDistanceWithCompoundPredicate(compoundPredicate: compound) { distance in
            completion(distance)
        }
    }


    func getDistanceRunning(completion: @escaping (Double) -> Void) {
        let workoutPredicate = HKQuery.predicateForWorkouts(with: .running)

        let compound = NSCompoundPredicate(andPredicateWithSubpredicates:
            [workoutPredicate, datePredicate])

        getDistanceWithCompoundPredicate(compoundPredicate: compound) { distance in
            completion(distance)
        }
    }

    private func getDistanceWithCompoundPredicate(compoundPredicate: NSCompoundPredicate,
                                          completion: @escaping (Double) -> Void) {
        let query = HKSampleQuery(sampleType: .workoutType(),
                                  predicate: compoundPredicate,
                                  limit: 0,
                                  sortDescriptors: nil) { (query, samples, error) in
            DispatchQueue.main.async {
                guard let samples = samples as? [HKWorkout], error == nil else {
                    return completion(0)
                }
                let distance = samples.compactMap({ $0.totalDistance?.doubleValue(for: .meter()) }).reduce(0, +)
                completion(distance)
            }
        }

        HKHealthStore().execute(query)
    }
}
