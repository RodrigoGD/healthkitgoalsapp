//
//  GetGoalListUseCase.swift
//  HealthKitGoals
//
//  Created by Rodrigo Gálvez on 29/8/21.
//

import Foundation

class GetGoalListUseCase {

    private let goalRepository: GoalRepository!
    private let healthKitStore: HealthKitStore!

    init(_ goalRepository: GoalRepository = GoalRepositoryImplementation(),
         _ healthKitStore: HealthKitStore = HealthKitStore.sharedInstance) {
        self.goalRepository = goalRepository
        self.healthKitStore = healthKitStore
    }

    // MARK: - Goal repository
    func execute(completion: @escaping (Result<[Goal], DomainError>) -> Void) {
        goalRepository.getGoals { result in
            completion(result.mapError { error in
                DomainError(message: "Failed to retrieve goal list")
            })
        }
    }

    // MARK: - HealthKit store
    func requestHealthKitAuthorization(completion: @escaping(Result<Bool, DomainError>) -> Void) {
        healthKitStore.authorizeHealthKit { result in
            switch result {
            case .success:
                completion(.success(true))
            case .failure:
                completion(.failure(DomainError(message: "HealthKit data authorization was failed")))
            }
        }
    }
}
