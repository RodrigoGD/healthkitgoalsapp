//
//  GetGoalUseCase.swift
//  HealthKitGoals
//
//  Created by Rodrigo Gálvez on 29/8/21.
//

import Foundation

class GetGoalUseCase {

    private let goalRepository: GoalRepository!
    private let healthKitStore: HealthKitStore!

    init(_ goalRepository: GoalRepository = GoalRepositoryImplementation(),
         _ healthKitStore: HealthKitStore = HealthKitStore.sharedInstance) {
        self.goalRepository = goalRepository
        self.healthKitStore = healthKitStore
    }

    // MARK: - Goal repository
    func execute(id: Int, completion: @escaping (Result<Goal, DomainError>) -> Void) {
        goalRepository.getGoal(id: id) { result in
            completion(result.mapError { error in
                DomainError(message: "Failed to retrieve goal details")
            })
        }
    }

    // MARK: - HealthKit store
    func getProgressForGoalType(goalType: GoalType, completion: @escaping (Double) -> Void) {
        switch goalType {
        case .step:
            healthKitStore.getStepCount { value in
                completion(value)
            }
        case .walk:
            healthKitStore.getDistanceWalking { value in
                completion(value)
            }
        case .run:
            healthKitStore.getDistanceRunning { value in
                completion(value)
            }
        }
    }

}
