//
//  GoalDetailDataView.swift
//  HealthKitGoals
//
//  Created by Rodrigo Gálvez on 29/8/21.
//

import Foundation

struct GoalDetailDataView {
    let title: String
    let description: String
    let progressValue: Float
    let progressDescription: String
    let typeImage: String
    let trophyImage: String
    let completed: Bool
}
