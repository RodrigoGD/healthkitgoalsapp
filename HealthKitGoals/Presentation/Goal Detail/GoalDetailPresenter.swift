//
//  GoalDetailPresenter.swift
//  HealthKitGoals
//
//  Created by Rodrigo Gálvez on 29/8/21.
//

import Foundation

protocol GoalDetailPresenter {
    var view: GoalDetailView? { get set }
    func viewWillAppear()
}

class GoalDetailPresenterImplementation {
    var view: GoalDetailView?
    private let getGoalUseCase: GetGoalUseCase
    private let goalId: Int

    init(getGoalUseCase: GetGoalUseCase = GetGoalUseCase(), goalId: Int) {
        self.getGoalUseCase = getGoalUseCase
        self.goalId = goalId
    }

    private func loadGoal(completion: @escaping (Goal) -> Void,
                          errorCompletion: @escaping (String) -> Void) {
        getGoalUseCase.execute(id: goalId) { result in
            switch result {
            case .success(let goal):
                completion(goal)
            case .failure(let error):
                errorCompletion(error.message)
            }
        }
    }

    private func loadProgressForGoal(_ type: GoalType, completion: @escaping (Double) -> Void) {
        getGoalUseCase.getProgressForGoalType(goalType: type) { result in
            completion(result)
        }
    }
}

extension GoalDetailPresenterImplementation: GoalDetailPresenter {
    func viewWillAppear() {
        loadGoal { goal in
            self.loadProgressForGoal(goal.type) { progress in
                guard let goalFloatValue = Float(goal.goal) else {
                    self.view?.showError("Cannot retrieve data")
                    return
                }
                let progressValue = Float(progress) / goalFloatValue
                print("\(progressValue)")
                let progressDescription = "\(progress) / \(goal.goal)"
                let goalDetailDataView = GoalDetailDataView(title: goal.title,
                                                            description: goal.description,
                                                            progressValue: progressValue,
                                                            progressDescription: progressDescription,
                                                            typeImage: goal.type.rawValue,
                                                            trophyImage: goal.reward.trophy.rawValue,
                                                            completed: false)

                self.view?.showGoalInfo(goalDetailDataView)
            }
        } errorCompletion: { error in
            self.view?.showError(error)
        }


    }
}
