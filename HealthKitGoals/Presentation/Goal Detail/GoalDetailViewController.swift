//
//  GoalDetailViewController.swift
//  HealthKitGoals
//
//  Created by Rodrigo Gálvez on 29/8/21.
//

import Foundation
import UIKit

protocol GoalDetailView {
    func showGoalInfo(_ goal: GoalDetailDataView)
    func showError(_ message: String)
}

class GoalDetailViewController: UIViewController {
    var goalId: Int? = nil
    private var presenter: GoalDetailPresenter? = nil

    @IBOutlet var typeImage: UIImageView!
    @IBOutlet var trophyImage: UIImageView!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var progressLabel: UILabel!
    @IBOutlet var progressView: UIProgressView!

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = GoalDetailPresenterImplementation(goalId: goalId ?? -1)
        presenter?.view = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.viewWillAppear()
    }
}

extension GoalDetailViewController: GoalDetailView{
    func showGoalInfo(_ goal: GoalDetailDataView) {
        title = goal.title
        typeImage.image =  UIImage(named: goal.typeImage)
        descriptionLabel.text = goal.description
        progressLabel.text = goal.progressDescription
        progressView.progress = goal.progressValue
        trophyImage.image = UIImage(named: goal.trophyImage)
    }

    func showError(_ message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(okAction)
        present(alert, animated: false)
    }
}
