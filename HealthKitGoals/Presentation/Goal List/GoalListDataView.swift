//
//  GoalListDataView.swift
//  HealthKitGoals
//
//  Created by Rodrigo Gálvez on 29/8/21.
//

struct GoalDataView {
    let id: Int
    let title: String
    let goal: String
    let image: String
}
