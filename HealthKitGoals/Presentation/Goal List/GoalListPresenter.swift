//
//  GoalListPresenter.swift
//  HealthKitGoals
//
//  Created by Rodrigo Gálvez on 29/8/21.
//

import Foundation

protocol GoalListPresenter {
    var view: GoalListView? { get set }
    func viewDidLoad()
    func viewWillAppear()
    func getGoalsCount() -> Int
    func getGoalByPosition(_ position: Int) -> GoalDataView?
}

class GoalListPresenterImplementation {
    var view: GoalListView?
    private let getGoalListUseCase: GetGoalListUseCase
    private var goals = [Goal]()

    init(getGoalListUseCase: GetGoalListUseCase = GetGoalListUseCase()) {
        self.getGoalListUseCase = getGoalListUseCase
    }

    private func loadGoals() {
        getGoalListUseCase.execute { result in
            switch result {
            case .success(let goals):
                self.goals = goals
                self.view?.reload()
            case .failure(let error):
                print(error)
            }
        }
    }

    private func requestAuthorization() {
        getGoalListUseCase.requestHealthKitAuthorization { result in
            if case let .failure(error) = result { print(error.localizedDescription) }
        }
    }
}

extension GoalListPresenterImplementation: GoalListPresenter {
    func viewDidLoad() {
        requestAuthorization()
    }

    func viewWillAppear() {
        loadGoals()
    }

    func getGoalsCount() -> Int {
        return goals.count
    }

    func getGoalByPosition(_ position: Int) -> GoalDataView? {
        guard position >= 0, position < goals.count else { return nil }
        let goal = goals[position]
        return GoalDataView(id: goal.id, title: goal.title, goal: goalSubtitle(goal), image: goal.type.rawValue)
    }

    private func goalSubtitle(_ goal: Goal) -> String {
        switch goal.type {
        case .step:
            return "\(goal.goal) steps"
        case .walk:
            return "Walk for \(Double(goal.goal)! / 1000.0)km"
        case .run:
            return "Run for \(Double(goal.goal)! / 1000.0)km"
        }
    }
}
