//
//  MainViewController.swift
//  HealthKitGoals
//
//  Created by Rodrigo Gálvez on 28/8/21.
//

import UIKit

protocol GoalListView {
    func reload()
}

class GoalListViewController: UIViewController {

    @IBOutlet var collectionView: UICollectionView!

    private var presenter: GoalListPresenter = GoalListPresenterImplementation()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.viewWillAppear()
    }

    private func setupView() {
        title = "HealthKit Goals"
        presenter.view = self
        presenter.viewDidLoad()
        setupTableView()
    }

    private func setupTableView() {
        collectionView.register(GoalCollectionViewCell.self,
                                forCellWithReuseIdentifier: GoalCollectionViewCell.cellIdentifier)
        collectionView.register(GoalCollectionViewCell.nib,
                                forCellWithReuseIdentifier: GoalCollectionViewCell.cellIdentifier)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
}

extension GoalListViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.getGoalsCount()
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GoalCollectionViewCell.cellIdentifier,
                                                            for: indexPath) as? GoalCollectionViewCell else {
            return UICollectionViewCell()
        }

        if let dataView = presenter.getGoalByPosition(indexPath.item) {
            cell.setupView(dataView)
        }

        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.black.cgColor

        return cell
    }
}

extension GoalListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let goal = presenter.getGoalByPosition(indexPath.item) else { return }
        navigateToGoalDetail(goal.id)
    }

    private func navigateToGoalDetail(_ goalId: Int) {
        let storyBoard = UIStoryboard(name: "GoalDetail", bundle: nil)
        if let controller = storyBoard.instantiateViewController(identifier: "GoalDetailViewController")
            as? GoalDetailViewController {
            controller.goalId = goalId
            navigationController?.pushViewController(controller, animated: true)
        }
    }
}

extension GoalListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width / 2.0 - 10
        let height = width
        return CGSize(width: width, height: height)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

extension GoalListViewController: GoalListView {
    func reload() {
        collectionView.reloadData()
    }
}
