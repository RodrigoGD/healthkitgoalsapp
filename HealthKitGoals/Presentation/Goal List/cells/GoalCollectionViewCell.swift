//
//  GoalCollectionViewCell.swift
//  HealthKitGoals
//
//  Created by Rodrigo Gálvez on 29/8/21.
//

import UIKit

class GoalCollectionViewCell: UICollectionViewCell {

    @IBOutlet var typeImage: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var goalLabel: UILabel!

    static let cellIdentifier = "GoalCollectionViewCell"
    static let nib = UINib(nibName: "GoalCollectionViewCell", bundle: nil)

    override class func awakeFromNib() {
        super.awakeFromNib()
    }

    func setupView(_ dataView: GoalDataView) {
        typeImage.image = UIImage(named: dataView.image)
        titleLabel.text = dataView.title
        goalLabel.text = dataView.goal
    }
}
