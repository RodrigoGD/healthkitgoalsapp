//
//  GoalFileManagerMock.swift
//  HealthKitGoalsTests
//
//  Created by Rodrigo Gálvez on 29/8/21.
//

@testable import HealthKitGoals
import Foundation

class GoalFileManagerMock: GoalFileManager {

    var retrieveSuccessful: Bool?

    override func retrieveGoals(completion: @escaping (Result<[GoalDto], FileManagerError>) -> Void) {
        guard let isSuccessful = retrieveSuccessful, isSuccessful else {
            return completion(.failure(.read))
        }

        completion(.success(GoalDto.listMock().goals))
    }
}

