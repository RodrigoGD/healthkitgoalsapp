//
//  GoalApiMock.swift
//  HealthKitGoalsTests
//
//  Created by Rodrigo Gálvez on 28/8/21.
//

@testable import HealthKitGoals
import Foundation

class GoalApiMock: GoalApi {

    var requestSuccessful: Bool?

    override func requestGoals(completion: @escaping ((Result<GoalListContainer, ApiError>) -> Void)) {
        guard let isSuccessful = requestSuccessful, isSuccessful else {
            return completion(.failure(.dataParsing))
        }

        completion(.success(GoalDto.listMock()))
    }
}
