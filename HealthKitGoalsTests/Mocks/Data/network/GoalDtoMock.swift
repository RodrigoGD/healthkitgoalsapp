//
//  GoalDtoMock.swift
//  HealthKitGoalsTests
//
//  Created by Rodrigo Gálvez on 28/8/21.
//

@testable import HealthKitGoals
import Foundation

extension GoalDto {
    static func testJson() -> Data {
        return """
                {
                "id": 157,
                "goal": "10000",
                    "description": "Mock description for the goal",
                    "title": "A title for a mock object",
                    "type": "step",
                    "reward": {
                        "trophy": "silver_medal",
                        "points": "999"
                    }
                }
                """.data(using: .utf8)!
    }

    static func mock() -> GoalDto {
        return GoalDto(id: 1, goal: "A goal", description: "A description",
                       title: "A title", type: .runningDistance,
                       reward: RewardDto(trophy: .zombieHand, points: "8723"))
    }

    static func listMock() -> GoalListContainer {
        return GoalListContainer(goals: [
            GoalDto(id: 1,
                    goal: "1000",
                    description: "First description",
                    title: "First title",
                    type: .step,
                    reward: RewardDto(trophy: .bronzeMedal,
                                      points: "10")),
            GoalDto(id: 2,
                    goal: "2000",
                    description: "Second description",
                    title: "Second title",
                    type: .walkingDistance,
                    reward: RewardDto(trophy: .silverMedal,
                                      points: "20")),
            GoalDto(id: 3,
                    goal: "3000",
                    description: "Third description",
                    title: "Third title",
                    type: .step,
                    reward: RewardDto(trophy: .goldMedal,
                                      points: "30")),
            GoalDto(id: 4,
                    goal: "4000",
                    description: "Fourth description",
                    title: "Fourth title",
                    type: .runningDistance,
                    reward: RewardDto(trophy: .zombieHand,
                                      points: "40"))
        ])
    }

}
