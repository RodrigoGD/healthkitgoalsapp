//
//  GoalMock.swift
//  HealthKitGoalsTests
//
//  Created by Rodrigo Gálvez on 28/8/21.
//

@testable import HealthKitGoals

extension Goal: Equatable {
    public static func == (lhs: Goal, rhs: Goal) -> Bool {
        if lhs.id == rhs.id,
            lhs.goal == rhs.goal,
            lhs.description == rhs.description,
            lhs.title == rhs.title,
            lhs.type == rhs.type,
            lhs.reward == rhs.reward {
            return true
        }
        return false
    }
}


extension Reward: Equatable {
    public static func == (lhs: Reward, rhs: Reward) -> Bool {
        if lhs.trophy == rhs.trophy,
            lhs.points == rhs.points {
            return true
        }
        return false
    }
}
