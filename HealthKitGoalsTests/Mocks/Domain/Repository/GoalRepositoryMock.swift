//
//  GoalRepositoryMock.swift
//  HealthKitGoalsTests
//
//  Created by Rodrigo Gálvez on 29/8/21.
//

@testable import HealthKitGoals

class GoalRepositoryMock: GoalRepository {

    var executionSuccessful: Bool?

    func getGoals(completion: @escaping (Result<[Goal], DomainError>) -> Void) {
        guard let isSuccessful = executionSuccessful, isSuccessful else {
            return completion(.failure(DomainError(message: "Unable to retrieve data")))
        }

        completion(.success(resultData()))
    }

    func getGoal(id: Int, completion: @escaping (Result<Goal, DomainError>) -> Void) {
        guard let isSuccessful = executionSuccessful, isSuccessful else {
            return completion(.failure(DomainError(message: "Unable to retrieve data")))
        }

        let resultData = resultData()
        guard id - 1 < resultData.count else {
            return completion(.failure(DomainError(message: "Goal not found")))
        }
        completion(.success(resultData[id - 1]))
    }

    private func resultData() -> [Goal] {
        return [
            Goal(id: 1, goal: "A goal", description: "A description", title: "A title",
                 type: .run, reward: Reward(trophy: .silver, points: "40")),
            Goal(id: 2, goal: "Another goal", description: "Another description", title: "Another title",
                 type: .walk, reward: Reward(trophy: .zombie, points: "999"))
        ]
    }
}
