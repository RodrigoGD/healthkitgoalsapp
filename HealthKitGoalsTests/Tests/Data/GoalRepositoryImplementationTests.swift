//
//  GoalRepositoryImplementationTests.swift
//  HealthKitGoalsTests
//
//  Created by Rodrigo Gálvez on 29/8/21.
//

@testable import HealthKitGoals
import XCTest

class GoalRepositoryImplementationTests: XCTestCase {

    var sut: GoalRepositoryImplementation!
    var goalApiMock: GoalApiMock!
    var goalFileManagerMock: GoalFileManagerMock!

    override func setUpWithError() throws {
        goalApiMock = GoalApiMock()
        goalFileManagerMock = GoalFileManagerMock()
        sut = GoalRepositoryImplementation(goalApiMock, goalFileManagerMock)
    }

    override func tearDownWithError() throws {
        sut = nil
        goalApiMock = nil
        goalFileManagerMock = nil
    }

    func testGetGoalsSuccess() throws {
        goalApiMock.requestSuccessful = true
        let expectedGoals = [
            Goal(id: 1, goal: "1000", description: "First description",
                 title: "First title", type: .step, reward: Reward(trophy: .bronze, points: "10")),
            Goal(id: 2, goal: "2000", description: "Second description",
                 title: "Second title", type: .walk, reward: Reward(trophy: .silver, points: "20")),
            Goal(id: 3, goal: "3000", description: "Third description",
                 title: "Third title", type: .step, reward: Reward(trophy: .gold, points: "30")),
            Goal(id: 4, goal: "4000", description: "Fourth description",
                 title: "Fourth title", type: .run, reward: Reward(trophy: .zombie, points: "40"))
        ]

        sut.getGoals { result in
            switch result {
            case .success(let goals):
                XCTAssertTrue(goals.count == 4)
                XCTAssertEqual(goals, expectedGoals)
            case .failure:
                assertionFailure("Get goals request wasn't successful")
            }
        }
    }

    func testGetGoalsNetworkFailLocalSuccess() throws {
        goalApiMock.requestSuccessful = false
        goalFileManagerMock.retrieveSuccessful = true
        let expectedGoals = [
            Goal(id: 1, goal: "1000", description: "First description",
                 title: "First title", type: .step, reward: Reward(trophy: .bronze, points: "10")),
            Goal(id: 2, goal: "2000", description: "Second description",
                 title: "Second title", type: .walk, reward: Reward(trophy: .silver, points: "20")),
            Goal(id: 3, goal: "3000", description: "Third description",
                 title: "Third title", type: .step, reward: Reward(trophy: .gold, points: "30")),
            Goal(id: 4, goal: "4000", description: "Fourth description",
                 title: "Fourth title", type: .run, reward: Reward(trophy: .zombie, points: "40"))
        ]
        sut.getGoals { result in
            switch result {
            case .success(let goals):
                XCTAssertTrue(goals.count == 4)
                XCTAssertEqual(goals, expectedGoals)
            case .failure:
                assertionFailure("Get goals request was successful")
            }
        }
    }

    func testGetGoalsNetworkFailLocalFail() throws {
        goalApiMock.requestSuccessful = false
        goalFileManagerMock.retrieveSuccessful = false
        sut.getGoals { result in
            switch result {
            case .success:
                assertionFailure("Get goals request was successful")
            case .failure(let error):
                XCTAssertEqual(error.message, "Data cannot be retrieved")
            }
        }
    }

}
