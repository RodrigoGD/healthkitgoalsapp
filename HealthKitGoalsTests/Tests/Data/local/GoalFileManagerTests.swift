//
//  GoalFileManagerTests.swift
//  HealthKitGoalsTests
//
//  Created by Rodrigo Gálvez on 29/8/21.
//

@testable import HealthKitGoals
import XCTest

class GoalFileManagerTests: XCTestCase {

    var sut: GoalFileManager!

    override func setUpWithError() throws {
        sut = GoalFileManager(file: "goalTest.json")
    }

    override func tearDownWithError() throws {
        sut = nil
    }

    func testGoalFileManagerWriteAndRead() throws {
        let expectedGoals = GoalDto.listMock().goals

        sut.saveGoals(expectedGoals) { writeResult in
            switch writeResult {
            case .success:
                self.sut.retrieveGoals { readResult in
                    switch readResult {
                    case .success(let goals):
                        XCTAssertEqual(goals.count , expectedGoals.count)
                        XCTAssertEqual(goals.map { $0.toDomain() }, expectedGoals.map { $0.toDomain() })
                    case .failure:
                        assertionFailure("Should not fail when trying to write")
                    }
                }
            case .failure:
                assertionFailure("Should not fail when trying to write")
            }
        }
    }

}
