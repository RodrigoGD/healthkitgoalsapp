//
//  GoalDtoTests.swift
//  HealthKitGoalsTests
//
//  Created by Rodrigo Gálvez on 28/8/21.
//

@testable import HealthKitGoals


import XCTest

class GoalDtoTests: XCTestCase {

    func testGoalDtoParseFromJSON() {
        let goalDto = try? JSONDecoder().decode(GoalDto.self, from: GoalDto.testJson())

        XCTAssertNotNil(goalDto)
        XCTAssertEqual(goalDto?.id, 157)
        XCTAssertEqual(goalDto?.goal, "10000")
        XCTAssertEqual(goalDto?.description, "Mock description for the goal")
        XCTAssertEqual(goalDto?.title, "A title for a mock object")
        XCTAssertEqual(goalDto?.type, GoalTypeDto.step)
        XCTAssertEqual(goalDto?.reward.trophy, TrophyDto.silverMedal)
        XCTAssertEqual(goalDto?.reward.points, "999")
    }

    func testGoalDtoToDomain() {
        let expectedGoal = Goal(id: 1, goal: "A goal", description: "A description",
                                title: "A title", type: .run,
                                reward: Reward(trophy: .zombie, points: "8723"))
        let sut = GoalDto(id: 1, goal: "A goal", description: "A description",
                          title: "A title", type: .runningDistance,
                          reward: RewardDto(trophy: .zombieHand, points: "8723"))

        let domainModel = sut.toDomain()

        XCTAssertEqual(domainModel, expectedGoal)
    }

}
