//
//  GetGoalListUseCaseTests.swift
//  HealthKitGoalsTests
//
//  Created by Rodrigo Gálvez on 29/8/21.
//

import Foundation

@testable import HealthKitGoals
import XCTest

class GetGoalListUseCaseTests: XCTestCase {

    var sut: GetGoalListUseCase!
    var goalRepositoryMock: GoalRepositoryMock!

    override func setUpWithError() throws {
        goalRepositoryMock = GoalRepositoryMock()
        sut = GetGoalListUseCase(goalRepositoryMock)
    }

    override func tearDownWithError() throws {
        sut = nil
        goalRepositoryMock = nil
    }

    func testGetGoalsSuccess() throws {
        goalRepositoryMock.executionSuccessful = true
        let expectedGoals = [
            Goal(id: 1, goal: "A goal", description: "A description", title: "A title",
                 type: .run, reward: Reward(trophy: .silver, points: "40")),
            Goal(id: 2, goal: "Another goal", description: "Another description", title: "Another title",
                 type: .walk, reward: Reward(trophy: .zombie, points: "999"))
        ]

        sut.execute { result in
            switch result {
            case .success(let goals):
                XCTAssertTrue(goals.count == 2)
                XCTAssertEqual(goals, expectedGoals)
            case .failure:
                assertionFailure("Execution wasn't successful")
            }
        }
    }

    func testGetGoalsFail() throws {
        goalRepositoryMock.executionSuccessful = false

        sut.execute { result in
            switch result {
            case .success:
                assertionFailure("Execution was successful")
            case .failure(let error):
                XCTAssertEqual(error.message, "Failed to retrieve goal list")
            }
        }
    }

}
