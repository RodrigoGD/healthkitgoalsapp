//
//  GetGoalUseCaseTests.swift
//  HealthKitGoalsTests
//
//  Created by Rodrigo Gálvez on 29/8/21.
//

import Foundation

@testable import HealthKitGoals
import XCTest

class GetGoalUseCaseTests: XCTestCase {

    var sut: GetGoalUseCase!
    var goalRepositoryMock: GoalRepositoryMock!

    override func setUpWithError() throws {
        goalRepositoryMock = GoalRepositoryMock()
        sut = GetGoalUseCase(goalRepositoryMock)
    }

    override func tearDownWithError() throws {
        sut = nil
        goalRepositoryMock = nil
    }

    func testGetGoalsSuccess() throws {
        goalRepositoryMock.executionSuccessful = true
        let expectedGoal = Goal(id: 2, goal: "Another goal", description: "Another description",
                                title: "Another title", type: .walk,
                                reward: Reward(trophy: .zombie, points: "999"))

        sut.execute(id: 2) { result in
            switch result {
            case .success(let goal):
                XCTAssertEqual(goal, expectedGoal)
            case .failure:
                assertionFailure("Execution wasn't successful")
            }
        }
    }

    func testGetGoalFail() throws {
        goalRepositoryMock.executionSuccessful = false

        sut.execute(id: 1) { result in
            switch result {
            case .success:
                assertionFailure("Execution was successful")
            case .failure(let error):
                XCTAssertEqual(error.message, "Failed to retrieve goal details")
            }
        }
    }

    func testGetGoalNotFound() throws {
        goalRepositoryMock.executionSuccessful = false

        sut.execute(id: 12) { result in
            switch result {
            case .success:
                assertionFailure("Execution was successful")
            case .failure(let error):
                XCTAssertEqual(error.message, "Failed to retrieve goal details")
            }
        }
    }

}
